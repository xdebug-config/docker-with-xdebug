### INSTALLATION DOCKER PHP 7.3 XDEBUG
```
sudo docker build Dockerfiles/php-7.3-xdebug -t image-php-7.3-xdebug
sudo docker run --name container-php-7.3-xdebug  -v $PWD:/var/www -p 83:80 --rm -d image-php-7.3-xdebug

```
### INSTALLATION DOCKER PHP 7.3 ARTISAN XDEBUG
```
sudo docker build Dockerfiles/laravel-7.3-xdebug -t image-laravel-7.3-xdebug
sudo docker run --name container-laravel-7.3-xdebug -v $PWD:/var/www -p 82:80 --rm -d image-laravel-7.3-xdebug
```
